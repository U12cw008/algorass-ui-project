import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepartmentComponent } from './component/ListData/department/department.component';
import { BenefitComponent } from './component/ListData/benefit/benefit.component';
import { DepartmentAddComponent } from './component/AddData/department-add/department-add.component';
import { ApplicantComponent } from './component/ListData/applicant/applicant.component';
import { ApplicantAddComponent } from './component/AddData/applicant-add/applicant-add.component';
import { EmployeeComponent } from './component/ListData/employee/employee.component';
import { EmployeeAddComponent } from './component/AddData/employee-add/employee-add.component';


const routes: Routes = [
  { path: 'benefit', component: BenefitComponent },
  { path: 'department', component: DepartmentComponent },
  { path: 'departmentAdd', component: DepartmentAddComponent },
  { path: 'applicant', component: ApplicantComponent },
  { path: 'applicantAdd', component: ApplicantAddComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: 'employeeAdd', component: EmployeeAddComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
