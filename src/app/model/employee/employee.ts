import { Department } from '../department';

export class Employee{
    
	 id : Number;
	 empId : String;
	 empFirstName : String;
	 empMidleName : String;
	 emplLastName : String;
	 empGender : String;
	 empType : String;
	 isDeleted : Boolean;
	 empDateOfBirth: Date;
	 startDate: Date;
	 endDate: Date;
	 department :Department;
	 employeeImage:File[];

}