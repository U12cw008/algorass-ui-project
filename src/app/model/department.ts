   
   export class Department{
       
       id:number;
       departmentId:String;
       departmentName:String;
       departmentDescription:String;
       isDeleted :Boolean;
   }