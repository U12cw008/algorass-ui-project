
export class Applicant{

    id :number;
    firstName :String;
	lastName :String;
	address :String;
	city :String;
	country :String;
	phone :String;
	applicantType :Boolean;
}