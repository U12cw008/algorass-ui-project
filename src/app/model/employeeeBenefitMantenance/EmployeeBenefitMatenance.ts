import { Benefit } from '../Benefit/Benefit';

export class EmployeeBenefitMantenance{

     id :number;
	 benefitName :String;
	 startDate :Date;
	 endDate  :Date;
	 benefitAmountPerYear :number;
	 benefitAmountPerMonth  :number;
	 benefitCodeDto :Benefit;
}