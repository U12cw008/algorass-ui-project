import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Deduction } from '../../model/deduction/deduction';
import { ConstantConfig } from '../../../app/framework/constantCofig';

@Injectable({
    providedIn:'root'
})
export class DeductionService{

    deduction :Deduction =new Deduction();
    constructor(private httpClient :HttpClient){}

    private baseUrl_forGetAll = ConstantConfig.baseurl_ForDeduction +'/getAllDeduction';
    private baseUrl_forCreate = ConstantConfig.baseurl_ForDeduction +'/create';
    private baseUrl_forUpdate = ConstantConfig.baseurl_ForDeduction +'/update';
    private baseUrl_forDelete = ConstantConfig.baseurl_ForDeduction +'/delete';
    private base_UrlForSearch = ConstantConfig.baseurl_ForDeduction +'/searchDeduction';

    getAllDeductionCode(){
        return this.httpClient.get<Deduction[]>(this.baseUrl_forGetAll);
    }

    createDeduction(deduction){
        return this.httpClient.post<Deduction>(this.baseUrl_forCreate,deduction);
    }

    updateDeduction(deduction){
        return this.httpClient.post<Deduction>(this.baseUrl_forUpdate,deduction);
    }
    deleteeduction(deduction){
        return this.httpClient.post<Deduction>(this.baseUrl_forDelete,deduction);
    }

    searchDeduction(sort :string ,page: number, size: Number, body: any){
        const href= this.base_UrlForSearch;
        return this.httpClient.post<any>(href,body,{
            params: new HttpParams()
            .set('page',page.toString())
            .set('size',size.toString())
            .set('sort',sort.toString())
        });
    }

    setter(deduction :Deduction){
        this.deduction = deduction;
    }
    getter(){
        return this.deduction;
    }
}