import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Benefit } from '../../model/Benefit/Benefit';
import { ConstantConfig } from '../../../app/framework/constantCofig';

@Injectable({
    providedIn:'root'
})
export class BenefitService{
    benefit :Benefit =new Benefit();
    constructor(private httpClient:HttpClient){}

    private baseUrl_ForGetAll = ConstantConfig.baseUrl_ForBenefit + '/getAllDropDown';
    private baseUrl_ForCreate = ConstantConfig.baseUrl_ForBenefit + '/createBenefit';
    private baseUrl_ForUpdate = ConstantConfig.baseUrl_ForBenefit + '/updateBenefit';
    private baseUrl_ForDelete = ConstantConfig.baseUrl_ForBenefit + '/delete';
    private baseUrl_forsearch =  ConstantConfig.baseUrl_ForBenefit + '/searchBenefit';


    getAllBenefit(){
        return this.httpClient.get<Benefit[]>(this.baseUrl_ForGetAll);
    }

    createBenefit(benefit){
        return this.httpClient.post<Benefit>(this.baseUrl_ForCreate,benefit);
    }

    updateBenefit(benefit){
        return this.httpClient.post<Benefit>(this.baseUrl_ForUpdate,benefit);
    }

    deleteBenefit(benefit){
        return this.httpClient.post<Benefit>(this.baseUrl_ForDelete,benefit);
    }

    searchBenefit(sort :string ,page: number, size: Number, body: any){
        const href= this.baseUrl_forsearch;
        return this.httpClient.post<any>(href,body,{
            params: new HttpParams()
            .set('page',page.toString())
            .set('size',size.toString())
            .set('sort',sort.toString())
        });
    }

    setter(benefit : Benefit){
        this.benefit =benefit;
    }
    getter(){
        return this.benefit;
    }
}