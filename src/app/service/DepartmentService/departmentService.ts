import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Department } from '../../model/department';
import { ConstantConfig } from '../../../app/framework/constantCofig';


@Injectable({
    providedIn:'root'
})
export class DepartmentService{
    department:Department=new Department();
    constructor(private httpClient:HttpClient){}
    private base_UrlForGetAll=ConstantConfig.baseUre_ForDepartment +'/getAllDropDown';
    private base_UrlForCreate=ConstantConfig.baseUre_ForDepartment +'/create';
    private base_UrlForUpdate=ConstantConfig.baseUre_ForDepartment +'/update';
    private base_UrlForDelete=ConstantConfig.baseUre_ForDepartment +'/delete';
    private base_UrlForSearch = ConstantConfig.baseUre_ForDepartment + '/searchDepartment';



    getAllDepartment(){
        return this.httpClient.get<Department[]>(this.base_UrlForGetAll);
    }
    createDepartment(department){
        return this.httpClient.post<Department>(this.base_UrlForCreate,department);
    }

    updateDepartment(department){
        return this.httpClient.post<Department>(this.base_UrlForUpdate,department);
    }
    
    deleteDepartment(department){
        return this.httpClient.post<Department>(this.base_UrlForDelete,department);
    }

    searchDepartment(sort :string ,page: number, size: Number, body: any){
        const href= this.base_UrlForSearch;
        return this.httpClient.post<any>(href,body,{
            params: new HttpParams()
            .set('page',page.toString())
            .set('size',size.toString())
            .set('sort',sort.toString())
        });
    }


    setter(department:Department){
        this.department=department;
    }
    getter(){
        return this.department;
    }
}