import { Injectable } from '@angular/core';
import { EmployeeBenefitMantenance } from '../../model/employeeeBenefitMantenance/EmployeeBenefitMatenance';
import { HttpClient } from '@angular/common/http';
import { Benefit } from '../../../app/model/Benefit/Benefit';
import { ConstantConfig } from '../../../app/framework/constantCofig';

@Injectable({
    providedIn:'root'
})
export class EmployeeBenefitMantenanceService{

    employeeBenefitMantenance: EmployeeBenefitMantenance =new EmployeeBenefitMantenance();

    constructor(private httpClient :HttpClient){}

    private base_UrlForGetAll =ConstantConfig.baseurl_ForEmployeeBenefitMantenance +'/getAllDropDown';
    private base_UrlForCreate =ConstantConfig.baseurl_ForEmployeeBenefitMantenance +'/create';
    private base_UrlForUpdate =ConstantConfig.baseurl_ForEmployeeBenefitMantenance +'/update';
    private base_UrlForGetAllBenefitCode = ConstantConfig.baseUrl_ForBenefit + '/getAllDropDown';




        getAllDropDown(){
            return this.httpClient.get<EmployeeBenefitMantenance[]>(this.base_UrlForGetAll);
        }
        getAllCodeDropDown(){
            return this.httpClient.get<Benefit[]>(this.base_UrlForGetAllBenefitCode);
        }
        create(employeeBenefitMantenance){
            return this.httpClient.post<EmployeeBenefitMantenance>(this.base_UrlForCreate,employeeBenefitMantenance);
        }
        update(employeeBenefitMantenance){
            return this.httpClient.post<EmployeeBenefitMantenance>(this.base_UrlForUpdate,employeeBenefitMantenance);
        }
        
        setter(employeeBenefitMantenances :EmployeeBenefitMantenance){
             this.employeeBenefitMantenance =employeeBenefitMantenances;
        }
        getter(){
            return this.employeeBenefitMantenance;
        }

}