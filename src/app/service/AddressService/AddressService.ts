import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Address } from '../../model/address/address';
import { ConstantConfig } from '../../../app/framework/constantCofig';

@Injectable({
    providedIn:'root'
})
export class AddressService{

    address : Address = new Address();

    private baseUrl_ForCreate=ConstantConfig.baseUrl_ForAddress +'/create';
    private baseUrl_ForUpdate=ConstantConfig.baseUrl_ForAddress +'/update';
    private baseUrl_ForDelete=ConstantConfig.baseUrl_ForAddress +'/delete';
    private baseUrl_ForGetAllDropDown=ConstantConfig.baseUrl_ForAddress +'/getAllDropDown';
    private baseUrl_ForSearchAll=ConstantConfig.baseUrl_ForAddress+ '/searchData';

    constructor(private httpClient:HttpClient){}

    createAddress(address){
        return this.httpClient.post<Address>(this.baseUrl_ForCreate,address);
    }
    updateAddress(address){
        return this.httpClient.put<Address>(this.baseUrl_ForUpdate,address);
    }
    deleteAddress(address){
        return this.httpClient.post<Address>(this.baseUrl_ForDelete,address);
    }
    getAllAddress(){
        return this.httpClient.get<Address[]>(this.baseUrl_ForGetAllDropDown);
    }
    searchAddressData(sort: string, page: number,size: number, body: any){
        const href = this.baseUrl_ForSearchAll;
        return this.httpClient.post<any> (href,body,{
            params: new HttpParams()
            .set('page',page.toString())
            .set('size',size.toString())
            .set('sort',sort.toString())
        });
    }


    setter(address:Address){
        this.address=address;
    }
    getter(){
        return this.address;
    }
}