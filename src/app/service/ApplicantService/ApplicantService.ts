import { Injectable } from '@angular/core';
import { Applicant } from 'src/app/model/aplicant/Aplicant';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ConstantConfig } from '../../../app/framework/constantCofig';
import { Observable } from 'rxjs';


@Injectable({
    providedIn :'root'
})
export class ApplicantService{

    applicants :Applicant =new Applicant();
    constructor(private httpCient :HttpClient){}

    private baseUrl_forGetAll = ConstantConfig.baseurl_ForApplicant +'/getAllDropDown';
    private baseUrl_forCreate = ConstantConfig.baseurl_ForApplicant +'/create';
    private baseUrl_forUpdate = ConstantConfig.baseurl_ForApplicant +'/update';
    private baseUrl_ForDelete = ConstantConfig.baseurl_ForApplicant +'/delete';
    private baseUrl_ForSearchData = ConstantConfig.baseurl_ForApplicant +'/searchApplicant';
    
    getAllData(){
        return this.httpCient.get<Applicant[]>(this.baseUrl_forGetAll);
    }

    create(applicant){
         return this.httpCient.post<any>(this.baseUrl_forCreate,applicant);
    }

    update(applicant){
        return this.httpCient.post<any>(this.baseUrl_forUpdate,applicant);
    }

    delete(applicant){
        return this.httpCient.post<any>(this.baseUrl_ForDelete,applicant);
    }

    searchApplicant(sort: string, size: number,page: number,body: any){
        return this.httpCient.post<any>(this.baseUrl_ForSearchData,body,{
            params: new HttpParams()
                .set('page',page.toString())
                .set('size',size.toString())
                .set('sort', sort)
        });
    }

    setter(applicant :Applicant){
            this.applicants =applicant;
        }
    getter(){
        return this.applicants;
    }
}