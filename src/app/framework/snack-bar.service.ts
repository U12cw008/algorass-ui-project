import { Injectable, NgZone } from '@angular/core';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';


@Injectable({
  providedIn :'root'
})
export class SnackBarService {
    private config: MatSnackBarConfig;

    constructor(private snackbar: MatSnackBar, private zone: NgZone) {
      this.config = new MatSnackBarConfig();
      this.config.panelClass = ["snackbar-container"];
      this.config.verticalPosition = "bottom";
      this.config.horizontalPosition = "center";
      this.config.duration = 4000;
    }
  
    error(message: string) {
      this.config.panelClass = ["snackbar-container", "error"];
      this.show(message);
    }
  
    success(message: string) {
      this.config.panelClass = ["snackbar-container", "success"];
      this.show(message);
    }
    
    updated(message: string) {
      this.config.panelClass = ["snackbar-container", "success"];
      this.show(message);
    }

    delete(message: string) {
      this.config.panelClass = ["snackbar-container", "success"];
      this.show(message);
    }
  
    warning(message: string) {
      this.config.panelClass = ["snackbar-container", "warning"];
      this.show(message);
    }
  
    private show(message: string, config?: MatSnackBarConfig) {
      config = config || this.config;
      this.zone.run(() => {
        this.snackbar.open(message, "x", config);
      });
    }
  }