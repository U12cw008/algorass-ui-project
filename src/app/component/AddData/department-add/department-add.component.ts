import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Department } from 'src/app/model/department';
import { SnackBarService } from 'src/app/framework/snack-bar.service';
import { DepartmentService } from 'src/app/service/DepartmentService/departmentService';
import { retry } from 'rxjs/operators';

@Component({
  selector: 'app-department-add',
  templateUrl: './department-add.component.html',
  styleUrls: ['./department-add.component.css']
})
export class DepartmentAddComponent implements OnInit {

  department: Department = new Department();
  departmentForm: FormGroup;
  submitted= false;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private snackBarService:SnackBarService,
    private departmentService: DepartmentService
  ) {} 




  ngOnInit() {
    this.formValidator();
    this.department = this.departmentService.getter();
  }


  formValidator(){
    this.departmentForm = this.formBuilder.group({
      departmentId: ['',Validators.required],
      departmentName: ['',Validators.required],
      departmentDescription: ['' ,Validators.required]
    });
}

get f(){
  return this.departmentForm.controls;
}

processForm(){
  this.submitted = true;
  if(this.departmentForm.invalid){
    return;
  }
  if(this.department.id ===undefined){
    this.departmentService.createDepartment(this.department).subscribe(data =>{
      this.snackBarService.success('department has ben created successfully!');
      this.router.navigate(['/department']);
    });
  }else{
    this.departmentService.updateDepartment(this.department).subscribe(data =>{
      this.snackBarService.success('department has ben updated successfully!');
      this.router.navigate(['/department']);
    });
  }

}

onSubmit(){
  this.processForm();
}

cancelButton(){
  this.router.navigate(['/department']);
}
}