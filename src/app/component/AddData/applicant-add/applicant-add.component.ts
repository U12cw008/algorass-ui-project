import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ApplicantService } from '../../../../app/service/ApplicantService/ApplicantService';
import { Applicant } from '../../../model/aplicant/Aplicant';
import { SnackBarService } from 'src/app/framework/snack-bar.service';

@Component({
  selector: 'app-applicant-add',
  templateUrl: './applicant-add.component.html',
  styleUrls: ['./applicant-add.component.css']
})
export class ApplicantAddComponent implements OnInit {

  applicant :Applicant =new Applicant();
  submitted= false;
  applicantForm :FormGroup;

  constructor(private router :Router ,
    private applicantService:ApplicantService ,
    private formBuilder :FormBuilder,
    private snackBarService:SnackBarService) { }

  ngOnInit() {
    this.formValidator();
    this.applicant =this.applicantService.getter();
    
  }

  formValidator(){
    this.applicantForm =this.formBuilder.group({
      firstName :['' ,Validators.required],
      lastName :['' ,Validators.required],
      address :['',Validators.required],
      city :['' ,Validators.required],
      country :['' ,Validators.required],
      phone :['' ,Validators.required],
    });
  }
    
  get f() {
    return this.applicantForm.controls; 
  }

  processForm(){
    this.submitted =true;
    if(this.applicantForm.invalid){
      return;
    }
    if(this.applicant.id==undefined){
      this.applicantService.create(this.applicant).subscribe((applicant)=>{
        this.snackBarService.success("applicant created successfully!!");
          this.router.navigate(['/applicant']);
          
      })
    }else{
      this.applicantService.update(this.applicant).subscribe((applicant)=>{
        this.snackBarService.success("applicant updated successfully!!");
        this.router.navigate(['/applicant']);
      });
    }
  }

  onSubmit(){
    this.processForm();
  }


  cancelButton(){
    this.router.navigate(['/applicant']);
  }
}
