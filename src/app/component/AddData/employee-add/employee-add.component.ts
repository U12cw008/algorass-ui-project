import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../../../../app/model/employee/employee';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../../../../app/service/EmployeeService/employeeService';
import { SnackBarService } from '../../../../app/framework/snack-bar.service';
import { DateAdapter } from '@angular/material';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  employee : Employee = new Employee();
  submitted= false;
  employeeForm :FormGroup;
  file:File;
  today: Date;

  employees :any;
  constructor(private router: Router,
    private employeeService: EmployeeService,
    private formBuilder: FormBuilder,
    private snackBarService:SnackBarService,
    private dateAdapter: DateAdapter<Date>) { 
      this.today =new Date();
      this.dateAdapter.setLocale('en-GB'); 
    }

  ngOnInit() {
    this.formValidator();
    this.employee = this.employeeService.getter();
    this.getAllDepartmentId();

  }

  formValidator(){
    this.employeeForm = this.formBuilder.group({
      empId: ['', Validators.required],
      empFirstName : ['',Validators.required],
	    empMidleName : ['',Validators.required],
	    emplLastName : ['',Validators.required],
      empGender : ['',Validators.required],
      empDateOfBirth: ['',Validators.required],
      startDate: ['',Validators.required],
      endDate: ['',Validators.required],
      departmentId: ['',Validators.required]
    });
  }

  get f() {
    return this.employeeForm.controls; 
  }

  processForm(){
    this.submitted = true;
    if(this.employeeForm.invalid){
      return;
    }
    if(this.employee.id ===undefined){
      this.employeeService.createEmployee(this.employee,this.file).subscribe(employee =>{
        this.snackBarService.success("Employee created successfully!")
        this.router.navigate(['/employee']);
      });
    }else{
      this.employeeService.updateEmployee(this.employee,this.file).subscribe(employee =>{
        this.snackBarService.updated("Employee updated successfully!");
        this.router.navigate(['/employee']);
      });
    }
  }

  getAllDepartmentId(){
    this.employeeService.getAllDepartMentId().subscribe(data =>{
      this.employees =  data;
    })
  }

  onSubmit(){
    this.processForm();
  }

  cancelButton(){
    this.router.navigate(['/employee']);
  }


}
