import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ApplicantService } from '../../../../app/service/ApplicantService/ApplicantService';
import { Router } from '@angular/router';
import { Applicant } from '../../../../app/model/aplicant/Aplicant';
import { SnackBarService } from 'src/app/framework/snack-bar.service';
import { MatDialog, MatDialogRef, MatPaginator, MatSort } from '@angular/material';
import { AppConfirmService } from 'src/app/framework/app-confirm/app-confirm.service';
import {tap} from 'rxjs/operators';
type NewType = MatPaginator;
@Component({
  selector: 'app-applicant',
  templateUrl: './applicant.component.html',
  styleUrls: ['./applicant.component.css']
})
export class ApplicantComponent implements OnInit ,AfterViewInit{


  displayedColumns :string[]= ['firstName', 'lastName', 'address','country','city','phone','action'];
  dataSource ;
  applicants :any;



  sortColumn = 'firstName';
  sortDirection ='asc';
  pageSize =5;
  filterBy = {'keyword' : ''};
  
  @ViewChild(MatPaginator,{static: true}) paginator: NewType;
  @ViewChild(MatSort,{static: true}) sort: MatSort;

  constructor(private router :Router ,
      private applicantService :ApplicantService,
      private snackBarService:SnackBarService,
      private matDialog: MatDialog,
      private appConfirmService :AppConfirmService) { }

    ngOnInit() {
      //this.getAllDropDownData();
      this.searchApplicant(this.filterBy);

    }


  ngAfterViewInit():void {

    this.sort.sortChange.subscribe(data =>{
      this.sortColumn = data.active;
      this.sortDirection = data.direction;
      this.paginator.pageIndex = 0;
    });
    this.paginator.page.pipe(tap(() =>{
      this.searchApplicant(this.filterBy);
    })
    ).subscribe();


  }


    getAllDropDownData(){
        this.applicantService.getAllData().subscribe((data)=>{
          this.dataSource = data;
        });
    }

    newApplicant(){
    let applicant =new Applicant();
    this.applicantService.setter(applicant);
    this.router.navigate(['/applicantAdd']);
    }

    saveOrUpdateApplicant(applicant){
      this.applicantService.setter(applicant);
      this.router.navigate(['/applicantAdd']);
    }


    searchApplicant(filterBy){
      this.applicantService.searchApplicant(`${this.sortColumn},${this.sortDirection}`,this.pageSize, this.paginator.pageIndex,filterBy)
      .subscribe(data =>{
        this.dataSource = data.content;
        this.paginator.length = data.totalElements;
      });
    }
    

    onFilter(filterValue: string){
      this.filterBy.keyword = filterValue;
      this.paginator.pageIndex = 0;
      this.searchApplicant(this.filterBy);
    }


    deleteApplicant(applicant){
        const dialogRef = this.appConfirmService.confirm({
          title: 'Delete Applicant',
          message: 'Are you sure you want to delete applicant? ' 
        });
        dialogRef.subscribe(result => {
          if(result){
          this.applicantService.delete(applicant).subscribe(() =>{
            this.snackBarService.delete('applicant has been deleted suceesfully');
            this.dataSource.data.splice(this.dataSource.data.indexOf(result), 1);
             });
            }
          });
        
    }

}
