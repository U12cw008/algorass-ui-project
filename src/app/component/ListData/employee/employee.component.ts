import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../../../service/EmployeeService/employeeService';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { tap } from 'rxjs/internal/operators/tap';
import { SnackBarService } from 'src/app/framework/snack-bar.service';
import { AppConfirmService } from 'src/app/framework/app-confirm/app-confirm.service';
import { Employee } from 'src/app/model/employee/employee';

type NewType = MatPaginator;
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit ,AfterViewInit{

  displayedColumns :string[]= ['empId','empFirstName','empMidleName','emplLastName','empGender' ,'action'];
  dataSource ;
  sortColumn ='empFirstName';
  sortDirection ='asc';
  pageSize =5;
  filterBy ={'keywor' :''};

  @ViewChild(MatPaginator,{static: true}) paginator: NewType;
  @ViewChild(MatSort,{static: true})sort: MatSort;

  constructor(private router:Router ,private employeeService: EmployeeService,
      private snackBarService:SnackBarService,
      private matDialog: MatDialog,
      private appConfirmService :AppConfirmService) { }

  ngOnInit() {
    this.searchEmployee(this.filterBy);
    
  }


  ngAfterViewInit():void {

    this.sort.sortChange.subscribe(data =>{
      this.sortColumn = data.active;
      this.sortDirection = data.direction;
      this.paginator.pageIndex = 0;
    });
    this.paginator.page.pipe(tap(() =>{
      this.searchEmployee(this.filterBy);
    })
    ).subscribe();


  }


  searchEmployee(filterBy){
    this.employeeService.searchEmployeess(`${this.sortColumn},${this.sortDirection}`,this.pageSize, this.paginator.pageIndex,filterBy)
    .subscribe(data =>{
      this.dataSource = data.content;
      this.paginator.length = data.totalElements;
    });
    
  }

  onFilter(filterValue :string){
    this.filterBy.keywor = filterValue;
    this.paginator.pageIndex = 0;
    this.searchEmployee(this.filterBy);

  }

  deleteEmployee(employee){
    const dialogRef = this.appConfirmService.confirm({
      title: 'Delete Employee',
      message: 'Are you sure you want to delete employee? ' 
    });
    dialogRef.subscribe(result => {
      if(result){
      this.employeeService.deleteEmployee(employee).subscribe(() =>{
        this.snackBarService.delete('employee has been deleted suceesfully');
        this.dataSource.data.splice(this.dataSource.data.indexOf(result), 1);
         });
        }
      });
    
  }

  newEmployee(){
    let employee = new Employee();
    this.employeeService.setter(employee);
    this.router.navigate(['/employeeAdd']);
  }

  saveOrUpdateApplicant(employee){
    this.employeeService.setter(employee);
    this.router.navigate(['/employeeAdd']);
  }

  }