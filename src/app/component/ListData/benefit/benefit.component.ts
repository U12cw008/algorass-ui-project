import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { BenefitService } from 'src/app/service/BenefitService/BenefitService';

@Component({
  selector: 'app-benefit',
  templateUrl: './benefit.component.html',
  styleUrls: ['./benefit.component.css']
})
export class BenefitComponent implements OnInit {
  
  displayedColumns :string[]= ['benefitId', 'benefitCode', 'benefitDesc','action'];
  dataSource;

  constructor(private router:Router,private benefitService : BenefitService) { }

  ngOnInit() {
    this.getBenefit();

  }

  getBenefit(){
      this.benefitService.getAllBenefit().subscribe(data=>{
      this.dataSource = data;
    })
  }

}
