import {Component, OnInit, ViewChild} from '@angular/core';
import { DepartmentService } from 'src/app/service/DepartmentService/departmentService';
import { Router } from '@angular/router';
import { Department } from 'src/app/model/department';
import { SnackBarService } from 'src/app/framework/snack-bar.service';
import { MatDialog } from '@angular/material';
import { AppConfirmService } from 'src/app/framework/app-confirm/app-confirm.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
  displayedColumns: string[] = ['departmentId', 'departmentName','departmentDescription','action'];
  dataSource;
  departments:any;

  constructor(private departmentService:DepartmentService,private router:Router,
      private snackBarService:SnackBarService,
      private matDialog: MatDialog,
      private appConfirmService :AppConfirmService){}

  ngOnInit() {
     this.getAllDepartment();
  }

  
  
  getAllDepartment(){
    return this.departmentService.getAllDepartment().subscribe(data=>{
        this.dataSource=data;
    });
}


    deletetDepartment(department){
      const dialog = this.appConfirmService.confirm({
        title :'Delete Department',
        message: 'Are you sure you want to delete Department?'
      });
        dialog.subscribe(results =>{
          if(results){
            this.departmentService.deleteDepartment(department).subscribe(() =>{
              this.snackBarService.delete('Department deleted suceessfully!');
              this.router.navigate(['/department']);
            });
          }
        });
    }

    
  newDepartment(){
    let department = new Department();
    this.departmentService.setter(department);
    this.router.navigate(['/departmentAdd']);
  }

  saveOrUpdate(department){
    this.departmentService.setter(department);
    this.router.navigate(['/departmentAdd']);
  }

}

