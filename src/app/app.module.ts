import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http' 

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { BenefitComponent } from './component/ListData/benefit/benefit.component';
import { DepartmentComponent } from './component/ListData/department/department.component';
import { DepartmentAddComponent } from './component/AddData/department-add/department-add.component';
import { ApplicantComponent } from './component/ListData/applicant/applicant.component';
import { ApplicantAddComponent } from './component/AddData/applicant-add/applicant-add.component';
import { MaterialModule } from './framework/material.module';
import { AppConfirmModule } from './framework/app-confirm/app-confirm.module';
import { EmployeeComponent } from './component/ListData/employee/employee.component';
import { EmployeeAddComponent } from './component/AddData/employee-add/employee-add.component';

@NgModule({
  imports:      [ 
    BrowserModule, 
    BrowserAnimationsModule, 
    ReactiveFormsModule, 
    MaterialModule ,
    AppRoutingModule,
    HttpClientModule,
    AppConfirmModule
  ],
  

    
  declarations: [ 
	  AppComponent,
    SidenavComponent,
    BenefitComponent,
    DepartmentComponent,
    DepartmentAddComponent,
    ApplicantComponent,
    ApplicantAddComponent,
    EmployeeComponent,
    EmployeeAddComponent
  ],

  bootstrap:    [ AppComponent ]
})
export class AppModule { }
